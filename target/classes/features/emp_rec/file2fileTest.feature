@try1
Feature: Integration test demo
  As a Mulesoft user, I want to verify processing of files in Mulesoft

  Background:
    Given I delete project files from the Mulesoft simulator and the user dir

  Scenario: Positive scenario - use an input payload XML that matches the expected schema
    Given I transfer the payload file "data.xml" to Mulesoft landing directory
    Then I wait for the file "data.xml" to disappear from the landing folder
    And I verify the file "data.csv" exists in outbound directory

    And I copy the output file "data.csv" to the user dir

    And I verify the "data.csv" with "data.xml" using the "output_mapping.csv"

  Scenario: Negative scenario - use an input payload XML that does not match the expected schema
    Given I transfer the payload file "different_data.xml" to Mulesoft landing directory
    Then I wait for the file "different_data.xml" to disappear from the landing folder
    And I verify the file "different_data.xml" exists in error directory
    And I verify the file "different_data.csv" does not exist in outbound directory

  Scenario: Negative scenario - use an input payload XML that is malformed
    Given I transfer the payload file "malformed_data.xml" to Mulesoft landing directory
    Then I wait for the file "malformed_data.xml" to disappear from the landing folder
    And I verify the file "malformed_data.xml" exists in error directory
    And I verify the file "malformed_data.csv" does not exist in outbound directory







