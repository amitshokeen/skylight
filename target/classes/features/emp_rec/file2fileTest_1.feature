
Feature: Integration test of Worrkday to ClearView Interface
  As a Mulesoft user, I want to verify processing of files in Mulesoft

  Background:
    #Given I set "Interface" variable as "emp_rec"
    #And I set "OutputFile" variable as "emp_rec.csv"
    # >> I got to first delete files from the Mulesoft simulator and the user dir
    Given I delete project files from the Mulesoft simulator and the user dir

  Scenario: Run positive scenario

    Given I transfer the payload file "data.xml" to Mulesoft landing directory
    Then I wait for the file "data.xml" to disappear from the landing folder
    And I verify the file "output.csv" exists in outbound directory

    And I copy the output file "output.csv" to the user dir

    And I verify the "output1.csv" with "data.xml" using the "output_mapping.csv"
    And I verify the output with input using the mapping file
      | Output File | Input File | Mapping File       |
      | output1.csv | data.xml   | output_mapping.csv |



