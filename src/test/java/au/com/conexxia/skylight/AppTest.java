package au.com.conexxia.skylight;

import org.junit.Assert;

import static org.junit.Assert.*;

public class AppTest {

    @org.junit.Test
    public void concatAndConvertString() {
        String expectedValue = "HELLOWORLD";
        App app=new App();
        String actualValue = app.concatAndConvertString("Hello", "World");
        assertEquals(expectedValue, actualValue);
        assertEquals(expectedValue, actualValue);
    }
}