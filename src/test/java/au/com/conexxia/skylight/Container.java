package au.com.conexxia.skylight;

public class Container {
    protected String interfaceName;
    protected String landingDir;
    protected String outboundDir;
    protected String errorDir;
    protected String outputFileName;

    public String getOutputFileName() {
        return outputFileName;
    }

    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    public String getErrorDir() {
        return errorDir;
    }

    public void setErrorDir(String errorDir) {
        this.errorDir = errorDir;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getLandingDir() {
        return landingDir;
    }

    public void setLandingDir(String landingDir) {
        this.landingDir = landingDir;
    }

    public String getOutboundDir() {
        return outboundDir;
    }

    public void setOutboundDir(String outboundDir) {
        this.outboundDir = outboundDir;
    }
}
