    package au.com.conexxia.skylight;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        glue = {"au.com.conexxia.skylight"},
        plugin = {"pretty", "html:target/cucumber-reports",
                "junit:target/test-reports.xml",
                "json:target/cucumber.json", "rerun:target/FailedScenarios.txt"}
        ,tags = "@try1"
)

public class CukeRunner {
//    public static void main(String[] args) throws Throwable {
//        String [] arguments = {};
//        cucumber.api.cli.Main.main(arguments);
//    }

}
