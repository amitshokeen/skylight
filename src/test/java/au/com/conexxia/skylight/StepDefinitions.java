package au.com.conexxia.skylight;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class StepDefinitions {
    protected Container container = new Container();


    @Given("^I set \"([^\"]*)\" variable as \"([^\"]*)\"$")
    public void i_set_Interface_variable_as(String variable, String value) throws Throwable {
        if(variable.equalsIgnoreCase("Interface")){
            //set the interface name and Mulesoft directories
            container.setInterfaceName(value);
            container.setLandingDir("/" + value + "/landing");
            container.setOutboundDir("/" + value + "/outbound");
            container.setErrorDir("/" + value + "/error");
        }
        else if (variable.equalsIgnoreCase("OutputFile")) {
            container.setOutputFileName(value);
        }
    }

    @Given("^I transfer the payload file \"([^\"]*)\" to Mulesoft landing directory$")
    public void i_transfer_the_payload_file_to_mulesoft_landing_directory(String payloadFile) throws Throwable {
        String payloadFileLocation = System.getProperty("user.dir");

        String payloadFileDestination = ReadConfig.getPropertyValue("MulesoftSimulator") + "\\"
                + ReadConfig.getPropertyValue("landingFolder");

        String payloadFileLocationAndName = payloadFileLocation + "\\" + payloadFile;

        String payloadFileDestinationAndName = payloadFileDestination + "\\" + payloadFile;

        FileOps.copyFileFromSourceToDestination(payloadFileLocationAndName, payloadFileDestinationAndName);

        Boolean isFilePresent = FileOps.isFilePresentInFolder(payloadFileDestination, payloadFile);

        Assert.assertTrue(isFilePresent);
    }

    @Then("^I wait for the file \"([^\"]*)\" to disappear from the landing folder$")
    public void i_Wait_For_The_File_To_Disappear_From_The_Landing_Folder(String fileName) throws Throwable {
        String filePath = ReadConfig.getPropertyValue("MulesoftSimulator") + "\\"
                + ReadConfig.getPropertyValue("landingFolder");

        boolean hasFileDisappeared = FileOps.waitForFileDisappear(fileName, filePath);

        Assert.assertTrue(hasFileDisappeared);
    }

    @And("^I verify the file \"([^\"]*)\" exists in outbound directory$")
    public void i_Verify_The_File_Exists_In_Outbound_Directory(String fileName) throws Throwable {
        String outputFileDestination = ReadConfig.getPropertyValue("MulesoftSimulator") + "\\"
                + ReadConfig.getPropertyValue("outboundFolder");

        Boolean isFilePresent = FileOps.isFilePresentInFolder(outputFileDestination, fileName);

        Assert.assertTrue(isFilePresent);
    }

//    @And("^I copy the output CSV file to MulesoftOutputVerification folder$")
//    public void i_Copy_The_Output_CSV_File_To_Mulesoft_Output_Verification_Folder() throws Throwable {
//        /****************************/
//        /****************************/
//
//
//        File source = new File("G:\\My Drive\\MulesoftSimulator\\outbound\\emp_rec.csv");
//        File dest = new File("C:\\Users\\amits\\Desktop\\MulesoftOutputVerification\\emp_rec.csv");
//        try {
//            FileUtils.copyFile(source, dest);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    @And("^I copy the output file \"([^\"]*)\" to the user dir$")
    public void i_Copy_The_Output_File_To_The_User_Dir(String outputFileName) throws Throwable {
        String outputFileLocation = ReadConfig.getPropertyValue("MulesoftSimulator") + "\\"
                + ReadConfig.getPropertyValue("outboundFolder");

        String outputFileLocationAndName = outputFileLocation + "\\" + outputFileName;

        String outputFileDestination = System.getProperty("user.dir");

        String outputFileDestinationAndName = outputFileDestination + "\\" + outputFileName;

        FileOps.copyFileFromSourceToDestination(outputFileLocationAndName, outputFileDestinationAndName);

        Boolean isFilePresent = FileOps.isFilePresentInFolder(outputFileDestination, outputFileName);

        Assert.assertTrue(isFilePresent);
    }

    @And("^I copy the payload XML file to MulesoftOutputVerification folder$")
    public void i_Copy_The_Pay_load_XML_File_To_Mulesoft_Output_Verification_Folder() throws Throwable {
        File source = new File("G:\\My Drive\\MulesoftSimulator\\processed\\emp_personal_rec.xml");
        File dest = new File("C:\\Users\\amits\\Desktop\\MulesoftOutputVerification\\emp_personal_rec.xml");
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @And("^I verify the \"([^\"]*)\" with \"([^\"]*)\" using the \"([^\"]*)\"$")
    public void i_Verify_The_Output_With_Input_Using_The_Mapping(String outputFile, String inputFile, String mappingFile) throws Throwable {
        List<String> booleans;
        //OutputChecker oc =  new OutputChecker("output.csv", "data.xml", "output_mapping.csv");
        //booleans = oc.validateOuputCSVFile();

        booleans = new OutputChecker("data.csv", "data.xml",
                "output_mapping.csv").validateOuputCSVFile();

        Boolean finalCombinedResult = false;

        for(String result : booleans){
            //finalCombinedResult = Boolean.valueOf(result) ? "true" : "false";
            if(Boolean.valueOf(result)){
                finalCombinedResult = true;
            } else finalCombinedResult = false;
        }


        System.out.println("*********" + booleans);

        Assert.assertTrue(finalCombinedResult);

    }

    @Given("^I delete project files from the Mulesoft simulator and the user dir$")
    public void i_Delete_Project_Files_From_The_Mulesoft_Simulator_And_The_User_Dir() throws Throwable {
        String mulesoftSimulatorPath = ReadConfig.getPropertyValue("MulesoftSimulator");

        String outboundFolder = ReadConfig.getPropertyValue("outboundFolder");
        String outboundFolderPath = mulesoftSimulatorPath + "\\" + outboundFolder;

        String landingFolder = ReadConfig.getPropertyValue("landingFolder");
        String landingFolderPath = mulesoftSimulatorPath + "\\" + landingFolder;

        String errorFolder = ReadConfig.getPropertyValue("errorFolder");
        String errorFolderPath = mulesoftSimulatorPath + "\\" + errorFolder;

        //FileOps.delete(outboundFolderPath, "output.csv");
        //FileOps.delete(landingFolderPath, "data.xml");
        FileOps.clean_directory_except_file_specified(outboundFolderPath, "desktop.ini");
        FileOps.clean_directory_except_file_specified(landingFolderPath, "desktop.ini");
        FileOps.clean_directory_except_file_specified(errorFolderPath, "desktop.ini");
        FileOps.delete(System.getProperty("user.dir"), "data.csv");
    }


    @And("^I verify the file \"([^\"]*)\" does not exist in outbound directory$")
    public void i_Verify_The_File_Does_Not_Exist_In_Outbound_Directory(String fileName) throws Throwable {
        // here I have to verify the file different_data.csv does not exist in the outbound directory
        String outboundFolder = ReadConfig.getPropertyValue("MulesoftSimulator") + "\\"
                + ReadConfig.getPropertyValue("outboundFolder");
        Boolean isFilePresent = FileOps.isFilePresentInFolder(outboundFolder, fileName);

        Assert.assertFalse(isFilePresent);
    }

    @And("^I verify the file \"([^\"]*)\" exists in error directory$")
    public void i_Verify_The_File_Exists_In_Error_Directory(String fileName) throws Throwable {
        // here I verify the file different_data.xml exists in the error directory

        String errorFileDestination = ReadConfig.getPropertyValue("MulesoftSimulator") + "\\"
                + ReadConfig.getPropertyValue("errorFolder");
        Boolean isFilePresent = FileOps.isFilePresentInFolder(errorFileDestination, fileName);

        Assert.assertTrue(isFilePresent);
    }
}
