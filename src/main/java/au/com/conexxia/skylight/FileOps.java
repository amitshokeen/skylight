package au.com.conexxia.skylight;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class FileOps {

//    public static String getPropertyValue(String property) throws IOException {
//        String propValue = null;
//        Properties prop = new Properties();
//        InputStream input = null;
//        input = new FileInputStream("config.properties");
//        prop.load(input);
//        propValue = prop.getProperty(property);
//        return propValue;
//    }

    public static void main(String[] args) throws IOException {
//        String payload = ReadConfig.getPropertyValue("payload");
//        System.out.println(payload);
//        clean_directory_except_file_specified("G:\\My Drive\\MulesoftSimulator\\landing", "desktop.ini");

    }

    public static void clean_directory_except_file_specified(String directoryPath, String fileName) {
        File dir = new File(directoryPath);
        for(File file: dir.listFiles()){
            if(!file.getName().equalsIgnoreCase(fileName)){
                file.delete();
            }
        }
    }

    public static String delete(String folderPath, String fileName) {
        File file = new File(folderPath + "\\" + fileName);
        if(file.exists()){
            if(file.delete()){
                return "file deleted";
            } else return "Error: file not deleted";
        } else return "file does not exist";
    }


    public static void copyFileFromSourceToDestination(String fileSource, String fileDestination){
        File source = new File(fileSource);
        //File dest = new File("G:\\My Drive\\MulesoftSimulator\\landing\\emp_personal_rec.xml");
        File dest = new File(fileDestination);
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isFilePresentInFolder(String folderPath, String fileName) {
        File f = new File(folderPath);
        ArrayList<String> fileNames = new ArrayList<String>(Arrays.asList(f.list()));
        return fileNames.contains(fileName);
    }

    public static boolean waitForFileDisappear(String fileName, String filePath) throws InterruptedException {
        if(!isFileStillPresent()){
            return true;
        }else return false;
    }

    private static boolean isFileStillPresent() throws InterruptedException {
        Thread.sleep(2*60*1000);
        return false;
    }
}
