package au.com.conexxia.skylight;

import java.sql.*;
import java.util.*;

public class CsvJDBC {
    private static Connection connection = null;
    private Statement statement;
    private static final String jdbcDriver = "org.relique.jdbc.csv.CsvDriver";
    private static String path = "C:\\Users\\amits\\Desktop\\MulesoftOutputVerification";

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName(jdbcDriver);
        Connection conn = DriverManager.getConnection("jdbc:relique:csv:" + path);
        Statement stmt = conn.createStatement();
        ResultSet results = stmt.executeQuery("SELECT outputColumn FROM emp_rec_outputMapping");
        while(results.next()){
            System.out.println(results.getString(1));
        }
        conn.close();
    }
}