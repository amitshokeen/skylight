package au.com.conexxia.skylight;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadConfig {

    public static String getPropertyValue(String property) throws IOException {
        String propValue = null;
        Properties prop = new Properties();
        InputStream input = null;
        input = new FileInputStream("config.properties");
        prop.load(input);
        propValue = prop.getProperty(property);
        return propValue;
    }
}
