package au.com.conexxia.skylight;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class OutputChecker {

    OutputChecker(String OUTPUT_CSV_FILE, String XML_DATA_FILE, String OUTPUT_MAPPING_CSV_FILE) {
        this.OUTPUT_CSV_FILE = OUTPUT_CSV_FILE;
        this.OUTPUT_CSV_FILE_NAME_AS_TABLE_NAME = this.OUTPUT_CSV_FILE.split("\\.(?=[^\\.]+$)")[0];

        this.XML_DATA_FILE = XML_DATA_FILE;

        this.OUTPUT_MAPPING_CSV_FILE = OUTPUT_MAPPING_CSV_FILE;
        this.OUTPUT_MAPPING_CSV_FILE_NAME_AS_TABLE_NAME = this.OUTPUT_MAPPING_CSV_FILE.split("\\.(?=[^\\.]+$)")[0];
    }

	// metadata file
	private String OUTPUT_MAPPING_CSV_FILE;
	private String OUTPUT_MAPPING_CSV_FILE_NAME_AS_TABLE_NAME;

	// data file
	private String XML_DATA_FILE;

	// output produced by system x
	private String OUTPUT_CSV_FILE;
	private String OUTPUT_CSV_FILE_NAME_AS_TABLE_NAME;

	private String CSV_JDBC_DRIVER = "org.relique.jdbc.csv.CsvDriver";
	private String CSV_JDBC_DB_CONNECTION_URL = "jdbc:relique:csv:" + System.getProperty("user.dir");
	
	private static final String NAME_OF_NODE_FOR_NODE_COUNT = "Worker";
	
	private static Integer TOTAL_NODE_COUNT = 0;

	private List<Map<String, Object>> outputMappingCSVList = null;
	private List<Map<String, Object>> outputCSVSingleRow = null;
	
	/*
	 * Method to be called externally - MAIN METHOD
	 */
	public List<String> validateOuputCSVFile() {
		List<String> response;

		this.setTotalNodeCount();
		response = new ArrayList<>();
		this.readOutputMappingFile();
		response = this.verifyOutputFile();

		return response;
	}

	/*
	 * Method with the business logic
	 */
	private List<String> verifyOutputFile() {
		List<String> response;
		String columnName;
		String valueFromXpath = "";
		String xpathFromMappingFile;
//		String serialNumberOfNode;
		String outputCSVQuery;
		String javaScriptCode;
		int offsetInQuery;
		ScriptEngineManager scriptEngineManager;
		ScriptEngine scriptEngine;
		Invocable invocable;
		response = new ArrayList<>();
		Pattern pattern;
		Matcher matcher;
		/*
		 * Considering output mapping file as the key for validating the output csv file
		 * by looking into the xml data and comparing xml <-> output.csv
		 * 
		 * This means if some row exists in the output.csv but was missed in
		 * output_mapping.csv it would be ignored
		 * 
		 * This also means the direction of validation is: output_mapping.csv ->
		 * data.xml -> output.csv; and even the row count of output_mapping.csv and
		 * output.csv are not validated
		 * 
		 */
		for(int i = 1; i <= TOTAL_NODE_COUNT; i++) {
		    javaScriptCode = null;
			for (Map<String, Object> outputMappingMap : this.outputMappingCSVList) {
	
				columnName = (String) outputMappingMap.get("outputColumn"); // column name is hard coded
				xpathFromMappingFile = (String) outputMappingMap.get("Xpath"); // column name is hard coded
				
				pattern = Pattern.compile("\\[.+?\\]");
				matcher = pattern.matcher(xpathFromMappingFile);
				xpathFromMappingFile = matcher.find() ? 
						matcher.replaceFirst("[" + i + "]") :
							xpathFromMappingFile;
						
						
				if("Translate".equals(xpathFromMappingFile)) {
					javaScriptCode = (String) outputMappingMap.get("Translate"); // column name is hard coded
//					System.out.println(javaScriptCode);
					javaScriptCode = reconstructJavaScriptCode(javaScriptCode, i);

				} else {
					valueFromXpath = readXPathFromXmlDataFile(xpathFromMappingFile);
				}
	

				offsetInQuery = i - 1;
					
				/*
				 *	Assuming the order of output_mapping.csv, data.xml and output.csv is the same
				 */
	
				outputCSVQuery = "SELECT * FROM " + OUTPUT_CSV_FILE_NAME_AS_TABLE_NAME
						+ " LIMIT 1 OFFSET " + offsetInQuery;

				
				this.getRowFromOutputFile(outputCSVQuery);
				
				
				if(null != javaScriptCode) {
			        scriptEngineManager = new ScriptEngineManager();
			        scriptEngine = scriptEngineManager.getEngineByName("JavaScript");
			        //javaScriptCode = StringUtils.substringBetween(javaScriptCode, "<", ">");
			        //System.out.println(javaScriptCode);
    		        try {
				        scriptEngine.eval(javaScriptCode);
				        invocable = (Invocable) scriptEngine;
				        //valueFromXpath = "1";
						//valueFromXpath = (String) invocable.invokeFunction("getValue", "Number");
                        valueFromXpath = (String) invocable.invokeFunction("getValue");
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					} catch (ScriptException e) {
						e.printStackTrace();
					}
				}
				
				/*
				 *	Assuming that we always have EQUALS comparison
				 */
				if(CollectionUtils.isNotEmpty(this.outputCSVSingleRow)  && 
					valueFromXpath.equals(this.outputCSVSingleRow.get(0).get(columnName))) {
					response.add(Boolean.TRUE.toString());
				} else {
//					System.out.println("XML data does not match with output file. Mapping file data {" + outputMappingMap + "}");
//					System.out.println("XML data does not match with output file. Output file data {" + this.outputCSVSingleRow + "}\n");
					response.add(Boolean.FALSE.toString() + " | The value {" + valueFromXpath +"} does not equal {" + this.outputCSVSingleRow.get(0).get(columnName) + "}");
				}
					
//				} else {
//					System.out.println(
//							"Problem with Xpath! Unable to identify row number! Xpath {" + xpathFromMappingFile + "}");
//					System.out.println("Making the match as false! Output mapping row {" + outputMappingMap + "}");
//					response.add(Boolean.FALSE.toString());
//				}
			}
		}
		
		return response;
	}

	//If the javascript code has embedded Xpaths, then this piece of code will evaluate those xpaths
    private String reconstructJavaScriptCode(String javaScriptCode, int workerNumber) {

        String value = "";
        Matcher matcher = Pattern.compile("<(.*?)>").matcher(javaScriptCode);
        List<String> xpaths = new ArrayList<>();
        while(matcher.find()){
            xpaths.add(matcher.group().toString());
        }
        for(int i = 0; i < xpaths.size(); i++){
            Map<String,String> rtnMap = null;
            String nodeXpath = xpaths.get(i).replace("<", "").replace(">", "");
            nodeXpath = nodeXpath.replace("*", String.valueOf(workerNumber));

            String nodeValue = "";
            nodeValue = readXPathFromXmlDataFile(nodeXpath);
            javaScriptCode = javaScriptCode.replace(xpaths.get(i), nodeValue);
        }

        return javaScriptCode;//reconstructed JavaScript Code;
    }
	
	private void setTotalNodeCount() {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc = null;
		InputStream inputStream = null;
		NodeList nodeList;
		try {
			// documentBuilderFactory.setNamespaceAware(true);
			documentBuilderFactory.setCoalescing(true);
			documentBuilderFactory.setIgnoringElementContentWhitespace(true);
			documentBuilderFactory.setIgnoringComments(true);
			builder = documentBuilderFactory.newDocumentBuilder();

            inputStream = new FileInputStream(XML_DATA_FILE);
			doc = builder.parse(inputStream);

			nodeList = doc.getElementsByTagName(NAME_OF_NODE_FOR_NODE_COUNT);
			TOTAL_NODE_COUNT = nodeList.getLength();
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		    if (inputStream != null) {
		        try {
		            inputStream.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
	}
	
	private String readXPathFromXmlDataFile(String xpathParam) {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc = null;
		InputStream inputStream = null;
		XPathFactory xpathFactory;
		XPath xpath;
		XPathExpression xPathExpression;
		String response = "";
		try {
			// documentBuilderFactory.setNamespaceAware(true);
			documentBuilderFactory.setCoalescing(true);
			documentBuilderFactory.setIgnoringElementContentWhitespace(true);
			documentBuilderFactory.setIgnoringComments(true);
			builder = documentBuilderFactory.newDocumentBuilder();

            inputStream = new FileInputStream(XML_DATA_FILE);
			doc = builder.parse(inputStream);
			
			// Create XPathFactory object
			xpathFactory = XPathFactory.newInstance();

			// Create XPath object
			xpath = xpathFactory.newXPath();

			/*
			 * http://www.baeldung.com/java-xpath
			 * https://www.w3.org/TR/2017/REC-xpath-31-20170321/#id-predicate
			 */
			xPathExpression = xpath.compile(xpathParam);

			response = (String) xPathExpression.evaluate(doc, XPathConstants.STRING);

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		    if (inputStream != null) {
		        try {
		            inputStream.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		return response;
	}

	// helper method to store output mapping csv in a list
	private void getRowFromOutputFile(String query) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultset = null;
		MapListHandler mapListHandler;
		try {
			DbUtils.loadDriver(CSV_JDBC_DRIVER);
			connection = DriverManager.getConnection(CSV_JDBC_DB_CONNECTION_URL);
			statement = connection.createStatement();
			resultset = statement.executeQuery(query);

			mapListHandler = new MapListHandler();
			this.outputCSVSingleRow = mapListHandler.handle(resultset);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeAndReleaseResources(resultset, statement, connection);
		}
	}

	// helper method to store output mapping csv in a list
	private void readOutputMappingFile() {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultset = null;

		try {
			DbUtils.loadDriver(CSV_JDBC_DRIVER);
			connection = DriverManager.getConnection(CSV_JDBC_DB_CONNECTION_URL);
			statement = connection.createStatement();
			resultset = statement.executeQuery("SELECT * FROM " + OUTPUT_MAPPING_CSV_FILE_NAME_AS_TABLE_NAME);

			MapListHandler mapListHandler = new MapListHandler();
			this.outputMappingCSVList = mapListHandler.handle(resultset);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeAndReleaseResources(resultset, statement, connection);
		}
	}
	
	// helper method to release all resources from CSV file as database
	private void closeAndReleaseResources(ResultSet resultset, Statement statement, Connection connection) {
		if (resultset != null) {
			try {
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if (statement != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


	/*private static void workerNumber_function() throws IOException {
        int workerNumber = 2;
	    String javaScriptCode = reconstructJavaScriptCode_try.readFile("myFile.txt");
        Matcher matcher = Pattern.compile("<(.*?)>").matcher(javaScriptCode);
        Matcher m;
        List<String> xpaths = new ArrayList<>();
        while(matcher.find()){
            xpaths.add(matcher.group().toString());
        }
        //for (int workerNumber=1; workerNumber <= 2; workerNumber++) {
            for (int i = 0; i < xpaths.size(); i++) {
                String nodeXpath = xpaths.get(i).replace("<", "").replace(">", "");
                nodeXpath = nodeXpath.replace("*", String.valueOf(workerNumber));

                //m = Pattern.compile("\\[.+?\\]").matcher(xpaths.get(i));
                //String x = m.find() ? m.replaceFirst("[" + workerNumber + "]") : xpaths.get(i);
                //xpaths.add(i, x);
                javaScriptCode = javaScriptCode.replace(xpaths.get(i), nodeXpath);
            }
        //}

    }*/
	/*
	 * Only to test if our code is working
	 * 
	 */
	public static void main(String[] args) throws IOException {
		System.out.println(new OutputChecker("output.csv", "data.xml",
              "output_mapping.csv").validateOuputCSVFile());
        //workerNumber_function();
	}

}
